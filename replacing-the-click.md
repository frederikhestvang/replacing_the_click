# Replacing the click

## Executive Summary
TBA 

## Introduction
At Densou Trading Desk we have spent the last 4 years buying programmatic display ads for some of the largest advertisers in the world. The benefits of programmatic buying includes endless targeting and reporting possibilities yet, in lack of an agreed-upon alternative, a key metric for programmatic campagins is still the click.

**With an average CTR of 0,1% for display ads, you are cutting 99,9% your display ads out of the picture if you are looking at any click-metric.**

We have spent a large part of the last 4 years making the case against the click and we want to share some of our findings with the world to create a global and more nuanced understanding of how reporting could be performed. This paper will be diveded into three sections:

1. Arguing why the click is such a sticky metric.
2. Introducing our 5 TB log-level dataset from our ad serving and DSP vendors and how we proccesed it.
3. Making the case against the click.
4. Suggesting alternative metrics.

## Definitions
This paper will use the following definitions.

* *Cost* Cost to advertiser.
* *CTR*
* *Land rate* 
* Intent to click
* *In screen time* Only MRA accredited ad servers
* *Cost per hour*

## The history of the click

### Search engines
* The first ad of the internet was a search ad.
* Altavista invented the PPC model.

### Analytics vendors
Started as analytics for PPC so naturally focus on on-site actions and click verification.

### Audiences
In the early days there was only one audience/target group. Internet geeks. The signal from search yields the other insights needed.

### Further research 
* eBay study on display ads
* Effect of TV on PPC
* Effect of display on PPC

## The Densou dataset
Get inspiration from the McKinsey report on company types.
Description of the sources:

1. Clickstream data
2. DSP log-level data
3. Google Analytics log exports

### The challenges of log-level joining
* Tiling: Which banner gave the click?

```
TODO: Insert image of tiling
```
* How to tell a non-land and subsequent visit from a real click-to-land.


### The challenges of large-level attribution
The issues above scales to the question of attribution but is beyond the scope of the paper.

## Issues with the click
Let us begin with observing a few paradoxes for a marketing manager looking to get as many clicks as possible and let us particularly see how an abnormally high level of clicks lead to a trade-off on the key metrics.

### Handling sparse data
Medicine, see Andrew Ngs

### Why land rates go together with click rates
Imagine a campaign with a CTR 100% and an land rate of 0%.
Ideally we want LR **and** to CTR be high

### Preliminary data analysis
We start by noting that the more inscreen-time a banner tends to get the higher the CTR is. It seems that seeing a banner for longer periods of time leads to that banner being clicked more as can be seen by the positve trends in the first plot.

```
TODO: Plot of (inscreen minutes, CTR) by device type
TODO: Plot of (inscreen minutes, land rate) by device type
```
Notice however that the land rate does not increase, rather it remains stable as the banner is in-screen for longer periods of time. We conclude that clicks from a banner being in-screen for a long time are as good as clicks from banners with less in-screen time.

This is an interesting observation since it shows that more in-screen time does to increase land rates at the same rate as CTR. The actual intent to click does not increase with in-screen time.

Continuing on this path consider this plot

```
TODO: Plot (CTR, LR og inscreen tid) på banner size
TODO: Sortér ikke efter alfabetisk banner størrelse, men inscreen tid
TODO: Fjern 1280x720
TODO: Split op, så én graf mobil (320x320, 320x160, 320x80)
TODO: Og ét andet på desktop (de øvrige)
```

Notice how the popular [FORMAT] has the lowest in-screen time but the highest land rate.

We will see these postive, negative and anti correlations between the clicks and other key metrics many more times in the following sections emphasizing the need for a metric that captures more than the clicks.

For the sake of completion and to cultivate a broad understanding we include similar plots split by other dimensions:

```
TODO: Plot (CTR, LR og inscreen tid) på device
TODO: Sortér ikke efter alfabetisk rækkefølge, men inscreen tid
TODO: Fjern TV og Unkown
```
``` javascript
[{"DeviceType":"Desktop and Laptop","share.imp":0.3684,"CTR":0.0019,"LR":0.4499,"AIS":12.84},{"DeviceType":"Mobile","share.imp":0.3653,"CTR":0.0018,"LR":0.3226,"AIS":1.68},{"DeviceType":"Tablet","share.imp":0.2613,"CTR":0.0026,"LR":0.1655,"AIS":5.49}] 
````

```
TODO: Plot (CTR, LR og inscreen tid) på client
TODO: Sortér ikke efter alfabetisk rækkefølge, men inscreen tid
TODO: Omdøb clients til client #1, client #2, client #3, etc.
```

```
TODO: Plot (CTR, LR og inscreen tid) på inventory
TODO: Sortér ikke efter alfabetisk rækkefølge, men inscreen tid
TODO: Omdøb inventories til inventory #1, inventory #2, etc
```





### Higher order visualisations
Having established a baseline understanding of the trade-offs one must to when evaluation a campaign we want to expand on this understanding. While a machine can capture differences like this easily the human mind breaks down when thiking about all the combinations of domains, SSPs, banners, advertisers, devices, etc

## Alternative metrics

### The cost per hour

1. Why you should use it but keep the CPC/CTR in this case
2. What you should ask your vendor (sampling)
3. What you should aim for

### The long-term effect

1. How you should meassure it
2. What are the issues
3. What are the solutions (Bayesian inference on baseline)
4. Densou Bayesian Classification Service.

### The CPX
